#!/usr/bin/env python3

import unittest
import pkg_resources
import numpy as np

from pydometer import Pedometer
from geneactiv import GENEActivBin  # needed to load example data

eg_filename = pkg_resources.resource_filename('pydometer', 'example_data/example.bin')


class TestPedometerInit(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.binfile = GENEActivBin(eg_filename)
        self.sr = (
            1.0
            / (
                self.binfile.accel.index[1] - self.binfile.accel.index[0]
            ).total_seconds()
        )

    def test_init_from_dataframe(self):
        p = Pedometer(data=self.binfile.accel)

    def test_init_from_dataframe_with_sr(self):
        p = Pedometer(data=self.binfile.accel, sr=self.sr)

    def test_init_from_dataframe_no_index(self):
        p = Pedometer(data=self.binfile.accel.reset_index(), sr=self.sr)

    def test_init_from_dataframe_no_index_no_sr(self):
        p = Pedometer(data=self.binfile.accel.reset_index())

    def test_init_from_arrays(self):
        p = Pedometer(
            gx=self.binfile.accel.gx.T,
            gy=self.binfile.accel.gy.T,
            gz=self.binfile.accel.gz.T,
        )

    def test_init_from_array_with_sr(self):
        p = Pedometer(
            gx=self.binfile.accel.gx.T,
            gy=self.binfile.accel.gy.T,
            gz=self.binfile.accel.gz.T,
            sr=self.sr,
        )


class TestStepCounters(unittest.TestCase):
    """The true step count in the example data is roughly 2000.  Make sure each step
    count method returns a value with the right order of magnitude.
    """

    @classmethod
    def setUpClass(self):
        self.binfile = GENEActivBin(eg_filename)
        self.expected_min_steps = 200
        self.expected_max_steps = 20000

    def test_matlab_counter(self):
        p = Pedometer(data=self.binfile.accel)
        step_count, step_locs = p.get_steps('matlab')
        self.assertTrue(step_count > self.expected_min_steps)
        self.assertTrue(step_count < self.expected_max_steps)

    def test_best_counter(self):
        p = Pedometer(data=self.binfile.accel)
        step_count, step_locs = p.get_steps('best')
        self.assertTrue(step_count > self.expected_min_steps)
        self.assertTrue(step_count < self.expected_max_steps)


if __name__ == '__main__':
    unittest.main()
